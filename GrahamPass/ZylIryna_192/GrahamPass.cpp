﻿#define _USE_MATH_DEFINES

#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>
#include <algorithm>


class Point
{
public:
	int X() const { return x_; }
	int Y() const { return y_; }

	Point(int x, int y)
	{
		x_ = x;
		y_ = y;
	}

	std::string Print() const {
		return std::to_string(x_) + " " + std::to_string(y_);
	}

private:
	int x_, y_;
};

class Stack
{
public:
	Stack(int max)
	{
		max_size_ = (max > general_max_size_) ? general_max_size_ : max;
	}

	/* Добавляет элемент в стек. Если стек полон, бросает исключение. */
	void Push(Point item)
	{
		if (stack_.size() == max_size_) {
			throw std::logic_error("There is already max amount of elements in the stack. ");
		}

		stack_.push_back(item);
	}

	/* Возвращает и удаляет элемент из стека. Если стек пуст, бросает исключение. */
	Point Pop()
	{
		if (IsEmpty()) {
			throw std::logic_error("Stack is empty. ");
		}

		Point top = Top();
		stack_.pop_back();

		return top;
	}


	/* Возвращает верхний элемент стека. */
	Point Top() const
	{
		if (IsEmpty()) {
			throw std::logic_error("Stack is empty. ");
		}

		return stack_[Size() - 1];
	}

	/* Возвращает предпоследний элемент стека. */
	Point NextToTop() const
	{
		if (stack_.size() <= 1) {
			throw std::logic_error("There are less than 2 elements in the stack. ");
		}

		return stack_[Size() - 2];
	}

	/* Размер стека. */
	int Size() const {
		return stack_.size();
	}

	/* Проверяет, пуст ли стек. */
	bool IsEmpty() const {
		return stack_.empty();
	}

private:
	const int general_max_size_ = INT32_MAX;
	std::vector<Point> stack_;
	int max_size_;
};


/* Проверяет, поворачиваем ли мы налево при добавлении новой точки.
   p0 - Точка, предпоследняя в стеке.
   p1 - Последняя точка в стеке.
   p2 - Новая точка.
   returns: Истина, если попорот налево, ложь, если поворот направо.
*/
bool IsLeftTurn(const Point& p0, const Point& p1, const Point& p2)
{
	int vectComp = (p1.X() - p0.X()) * (p2.Y() - p0.Y()) - (p2.X() - p0.X()) * (p1.Y() - p0.Y());
	return vectComp > 0;
}

/* Функция считает полярный угол для координат точки.
	x - Абсцисса точки.
	y - Координата точки.
	returns: Полярный угол в радианах.
*/
double Atan2(double x, double y)
{
	if (x == 0 && y == 0) {
		return 0;
	}
	if (x == 0 && y > 0) {
		return  M_PI / 2;
	}
	if (x == 0 && y < 0) {
		return 3 * M_PI / 2;
	}

	double res = atan(y / x);
	if (res < 0) {
		res += 2 * M_PI;
	}

	return res;
}

/* Компаратор, который сортирует точки по возрастанию полярного угла. */
bool PolarAngleComp(const Point& p1, const Point& p2) {
	return Atan2(p1.X(), p1.Y()) < Atan2(p2.X(), p2.Y());
}

/* Сортирует точки по полярному углу и удаляет лишние.
   pnts - Точки для сортировки.
   p0 - Точка, относительно которой сортируем.
   returns - Отсортированные точки.
*/
std::vector<Point> SortByPolarAngle(const std::vector<Point>& pnts, const Point& p0) {
	std::vector<Point> points;

	// Переводим в систему координат относительно р0.
	for (int i = 0; i < pnts.size(); i++) {
		points.emplace_back(pnts[i].X() - p0.X(), pnts[i].Y() - p0.Y());
	}

	// Сортируем по полярному углу.
	std::sort(points.begin(), points.end(), PolarAngleComp);

	std::vector<Point> answer;
	answer.push_back(points[0]);

	// Удаляем лишние элементы.
	for (int i = 1; i < points.size(); i++) {
		if (Atan2(answer.back().X(), answer.back().Y()) == Atan2(points[i].X(), points[i].Y())) {
			if (answer.back().X() * answer.back().X() + answer.back().Y() * answer.back().Y() <
				points[i].X() * points[i].X() + points[i].Y() * points[i].Y()) {
				answer.pop_back();
				answer.push_back(points[i]);
			}
		}
		else {
			answer.push_back(points[i]);
		}
	}

	// Возвращаем в исходную систему координат.
	for (int i = 0; i < answer.size(); i++) {
		answer[i] = Point(answer[i].X() + p0.X(), answer[i].Y() + p0.Y());
	}

	return answer;
}

/* Превращает стек в вектор.
   stack - Стек.
   returns: Список.
*/
std::vector<Point> StackToList(Stack* stack)
{
	std::vector<Point> answer;

	while (!(*stack).IsEmpty())
	{
		answer.push_back((*stack).Pop());
	}

	std::reverse(answer.begin(), answer.end());

	return answer;
}

/* Записывает ответ в формате Plain.
   pathout - Путь к файлу.
   answer - Выпуклая оболочка.
*/
void WriteToFilePlain(std::string pathout, std::vector<Point>& answer)
{
	answer.pop_back();

	std::ofstream out{ pathout };

	out << std::to_string(answer.size()) << "\n";

	for (int i = 0; i < answer.size(); i++) {
		out << answer[i].Print() << "\n";
	}
}

/* Записывает ответ в формате WKT.
   pathout - Путь к файлу.
   start - Изначальный список точек.
   answer - Выпуклая оболочка.
*/
void WriteToFileWKT(std::string pathout, const std::vector<Point>& start,
	const std::vector<Point>& answer)
{
	// Формируем изначальный ввод.
	std::string for_multi;
	for (int i = 0; i < start.size(); i++) {
		if (i == start.size() - 1) {
			for_multi += "(" + start[i].Print() + ")";
		}
		else {
			for_multi += "(" + start[i].Print() + "), ";
		}
	}

	// Формируем ответ.
	std::string for_poligon;
	for (int i = 0; i < answer.size(); i++) {
		if (i == answer.size() - 1) {
			for_poligon += answer[i].Print();
		}
		else {
			for_poligon += answer[i].Print() + ", ";
		}
	}

	std::string output = "MULTIPOINT (" + for_multi + ")\nPOLYGON ((" + for_poligon + "))";

	std::ofstream out{ pathout };
	out << output;
}

/* Компаратор, который сортирует точки по возрастанию y и x. */
bool MinPointComp(const Point& p1, const Point& p2) {
	if (p1.Y() == p2.Y()) {
		return p1.X() < p2.X();
	}

	return p1.Y() < p2.Y();
}

/* Обход Грэхема.
   pnts - Входные точки.
   returns: Выпуклая оболочка в виде стека.
*/
Stack GrahamPass(std::vector<Point> pnts)
{
	if (pnts.empty()) {
		throw std::logic_error("Empty points list. ");
	}

	// Ищем самую левую точку с минимальной координатой y.
	std::sort(pnts.begin(), pnts.end(), MinPointComp);
	Point p0 = pnts[0];

	// Сортируем точки по полярному углу.
	std::vector<Point> points = SortByPolarAngle(pnts, p0);
	points.push_back(p0);

	int m = points.size();

	if (points.size() <= 2) {
		throw std::logic_error("Сonvex hull is empty. ");
	}

	Stack S(m + 2);

	S.Push(p0);
	S.Push(points[0]);

	for (int i = 1; i < m; i++)
	{
		// Если три точки не образуют поворот налево, то удаляем последнюю точку стека.
		while (S.Size() > 1 && !IsLeftTurn(S.NextToTop(), S.Top(), points[i]))
		{
			S.Pop();
		}

		S.Push(points[i]);
	}

	return S;
}

int main(int argc, char* argv[])
{
	// Проверка на наличие входных данных.
	if (argc < 5) {
		std::cout << "You haven't inserted input and output files.";
		return -1;
	}

	// Пути к файлам входных и выходных данных.
	std::string pathin = argv[3];
	std::string pathout = argv[4];

	try {
		std::ifstream in{ pathin };

		// Проверка на существование файла входных данных.
		if (!in) {
			std::cout << "There is no such input file.";
			return -2;
		}

		// Читаем исходную строку из файла.
		int N = 0;
		in >> N;

		// Считываем точки.
		std::vector<Point> pnts;
		int x, y;
		for (int i = 0; i < N; i++) {
			in >> x >> y;
			pnts.emplace_back(x, y);
		}

		// Запускаем алгоритм.
		Stack stack = GrahamPass(pnts);

		// Формируем ответ.
		std::vector<Point> answer = StackToList(&stack);

		// Меняем порядок обхода, если надо.
		std::string order = argv[1];
		if (order == "cw") {
			std::reverse(answer.begin(), answer.end());
		}

		std::string format = argv[2];

		// Пишем ответ в файл.
		if (format == "plain") {
			WriteToFilePlain(pathout, answer);
		}
		else {
			WriteToFileWKT(pathout, pnts, answer);
		}
	}
	catch (std::exception& ex){
		std::cout << ex.what() << "\n";
	}

	return 0;
}